#!/usr/bin/env python
# encoding=utf-8
# The MIT License (MIT)
#
# Copyright (c) 2018 Bluewhale Robot
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Author: Randoms
#

from xiaoqiang_tts import _xunfei_tts
import rospy
from audio_common_msgs.msg import AudioData
import time
import mysql.connector
import sys


reload(sys)
sys.setdefaultencoding('utf8')


class XunfeiTTS:

    def __init__(self):
        self.app_id = rospy.get_param("~app_id", "")
        self.workdir = rospy.get_param("~work_dir", ".")
        self.timeout = rospy.get_param("~timeout", 5000)
        self.user_dict = rospy.get_param("~dict", "")

        connection = mysql.connector.connect(
            user='root',
            password='123456',
            host='127.0.0.1',
            database='test',
        )
        sql = "SELECT * FROM task"
        cursor = connection.cursor()
        cursor.execute(sql)
        rows = cursor.fetchall()
        self._tasks = ''
        for row in rows:
            print(str(row))
            (id, description) = row
            self._tasks += unicode(id) + '\n'
            self._tasks += str(description) + '\n'
        connection.commit()
        cursor.close()

    def tts(self, words):
        audio_data = AudioData()
        audio_data.data = _xunfei_tts.tts(words, self.app_id, self.workdir)
        return audio_data

    def asr(self, audio_data):
        res = _xunfei_tts.asr(
            [ord(x) for x in audio_data.data], self.app_id, self.user_dict, self.timeout, self.workdir)
        return res

    def ocwr(self, audio_data):
        res = _xunfei_tts.ocwr(
            [ord(x) for x in audio_data.data], self.app_id, self.user_dict, self.timeout, self.workdir, '任务\n电量\n' + self._tasks)
        return res
